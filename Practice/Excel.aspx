﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Excel.aspx.cs" Inherits="Practice.Excel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <asp:GridView ID="gvDemo" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Sex" HeaderText="Sex" SortExpression="Sex" />
                <asp:BoundField DataField="BirthDay" HeaderText="BirthDay" SortExpression="BirthDay" />
            </Columns>
        </asp:GridView>
    </p>
    <p>
        <asp:Button ID="btn_EPPlus_Export" runat="server" OnClick="btn_EPPlus_Export_OnClick"
            Text="EPPlus 导出" />
        <asp:Button ID="btn_Quick_Export" runat="server" OnClick="btn_Quick_Export_OnClick"
            Text="快速导出" />
    </p>
</asp:Content>
