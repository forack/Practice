﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WinFormPractice
{
    [ComVisible(true)]
    public partial class FormBrowserJs : Form
    {
        public FormBrowserJs()
        {
            InitializeComponent();

            webBrowser.Navigate(Path.Combine(Application.StartupPath, "HTMLBrowserJs.html"));
            webBrowser.ObjectForScripting = this;
        }

        public void ShowMsgForJs(string msg)
        {
            TBRecv.Text += $"{msg}\r\n";
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            webBrowser.Document.InvokeScript("ShowMsgForCSharp", new []{ TBSend.Text });
        }
    }
}
