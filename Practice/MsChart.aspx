﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MsChart.aspx.cs" Inherits="Practice.MsChartWebForm" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>   
    <form id="form1" runat="server">
    <div>
        <asp:Chart ID="Chart1" runat="server" Width="500px" BorderDashStyle="Solid" Palette="BrightPastel"
            ImageType="Png" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2"
            BackColor="#D3DFF0" BorderColor="26, 59, 105">
            <Titles>
                <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="折线图">
                </asp:Title>
            </Titles>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series Name="Series1" ChartType="Bubble" MarkerSize="8" MarkerStyle="Circle">
                </asp:Series>               
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                    BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                    <AxisX>
                        <MajorGrid Enabled="False"></MajorGrid>
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        
        <asp:Chart ID="Chart2" runat="server" Width="500px" BorderDashStyle="Solid" Palette="BrightPastel"
            ImageType="Png" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2"
            BackColor="#D3DFF0" BorderColor="26, 59, 105">
            <Titles>
                <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="样条图（平滑曲线）">
                </asp:Title>
            </Titles>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series Name="Series1" ChartType="Bubble" MarkerSize="8" MarkerStyle="Circle">
                </asp:Series>               
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                    BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientStyle="TopBottom">                    
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        
        <asp:Chart ID="Chart3" runat="server" Width="500px" BorderDashStyle="Solid" Palette="BrightPastel"
            ImageType="Png" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2"
            BackColor="#D3DFF0" BorderColor="26, 59, 105">
            <Titles>
                <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="条形图">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legends_Bar" DockedToChartArea="ChartArea1" Title="图例" TitleFont="Microsoft Sans Serif, 8pt" IsDockedInsideChartArea="False"></asp:Legend>
            </Legends>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series Name="Series1" ChartType="Bubble" MarkerSize="8" MarkerStyle="Circle">
                </asp:Series>               
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                    BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientStyle="TopBottom">                    
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        
        <asp:Chart ID="Chart4" runat="server" Width="500px" BorderDashStyle="Solid" Palette="BrightPastel"
            ImageType="Png" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2"
            BackColor="#D3DFF0" BorderColor="26, 59, 105">            
            <Titles>
                <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="饼图">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legends_Pie" DockedToChartArea="ChartArea1" Title="图例" TitleFont="Microsoft Sans Serif, 8pt"></asp:Legend>
            </Legends>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series Name="Series1" ChartType="Bubble" MarkerSize="8" MarkerStyle="Circle">
                </asp:Series>               
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                    BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientStyle="TopBottom">                    
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        
        <asp:Chart ID="Chart5" runat="server" Width="1000px" BorderDashStyle="Solid" Palette="BrightPastel"
            ImageType="Png" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2"
            BackColor="#D3DFF0" BorderColor="26, 59, 105">            
            <Titles>
                <asp:Title Font="微软雅黑, 16pt" Name="Title1" Text="饼图和雷达图">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" DockedToChartArea="ChartArea1" Title="图例1" IsDockedInsideChartArea="False"/>
                <asp:Legend Name="Legend2" DockedToChartArea="ChartArea2" Title="图例2" IsDockedInsideChartArea="False"/>
            </Legends>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series Name="Series1" ChartArea="ChartArea1" Legend="Legend1" ChartType="Point" MarkerSize="8" MarkerStyle="Circle"/>
                <asp:Series Name="Series2" ChartArea="ChartArea2" Legend="Legend2" ChartType="Radar" MarkerSize="0" MarkerStyle="Circle"/>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                    BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                </asp:ChartArea>
                <asp:ChartArea Name="ChartArea2" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                    BackColor="64, 165, 191, 228" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    </form>
</body>
</html>
