﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JS_Pager_Pagination_Test.aspx.cs" Inherits="Practice.JS_Pager_Pagination_Test" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pc-box visible-lg-block visible-md-block visible-sm-block">
        <!-- 列表开始 -->        
        <table width="100%" class="table table-bordered table-striped table-hover" id="table"></table>
        <!-- 列表结束 -->
        
        <!-- 翻页区开始 -->
        <div class="page-box"></div>
        <!-- 翻页区结束 -->
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FootContent" runat="server">
    
    <!-- 分页JS开始 -->
    <%--<script src="Scripts/jquery-1.4.1.js"></script>--%>
    <script src="Scripts/jquery-1.11.3.js"></script>
    <script src="Scripts/Pagination/jquery.pagination.js"></script>
    <script src="Scripts/Pagination/paging.js"></script>
    <script type="text/javascript">
        var perPageCount = 10;      // 每页展示数        
        var curPage = 1;            // 当前页

        // 初始化
        $(function () {
            loadData(curPage);
        });
        // 加载数据
        function loadData(page) {
            curPage=page;
            $.ajax({
                type: "post",
                dataType: "json",
                url: "/ashx/GetData.ashx",
                data: {                    
                    curPage:curPage,
                    perPageCount:perPageCount
                },
                success: function (data) {
                    var table = data.data;
                    var total = data.total;

                    if(data.msg == "1"){
                        var html = '<tr> <th style="width: 15%">订单编号</th> <th style="width: 30%">公司名称</th>' +
                            ' <th style="width: 20%">客户编号</th> <th style="width: 20%">雇员姓名</th> </tr>';
                        if (table.length > 0){
                            for (var i = 0; i < table.length; i++) {
                                var row = table[i];

                                html += '<tr><td>' + row.orderid + '</td>';
                                html += '<td>' + row.companyname + '</td>';
                                html += '<td>' + row.customerid + '</td>';
                                html += '<td>' + row.employeename + '</td></tr>';                                
                            }
                           
                            $("#table").html(html);
                            paging($(".page-box"), curPage, total, perPageCount, loadData);

                        }else {
                            $("#table").html("<p style='line-height: 50px;'>未查询到相应数据信息......</p>");
                            paging($(".page-box"), curPage, 0, perPageCount, loadData);
                        }
                    }
                
                },
                error: function () {
                }
            });
        }
    </script>
    <!-- 分页JS结束 -->

</asp:Content>
