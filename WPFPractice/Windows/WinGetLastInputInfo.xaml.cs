﻿using DotNet.Utilities.ConsoleHelper;
using DotNet.Utilities.WpfHelpers;
using PropertyChanged;
using System;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows;
using System.Windows.Input;

namespace WPFPractice.Windows
{
    public partial class WinGetLastInputInfo : Window
    {
        private GetLastInputInfoViewModel _vm;

        public WinGetLastInputInfo()
        {
            InitializeComponent();
            _vm = new GetLastInputInfoViewModel();
            DataContext = _vm;
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class GetLastInputInfoViewModel : BindableBase
    {
        #region 成员

        private System.Timers.Timer _timer = new Timer() { Interval = 200 };

        [DllImport("user32.dll")]
        static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        /// <summary>
        /// 上次输入信息结构体
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private struct LASTINPUTINFO
        {
            [MarshalAs(UnmanagedType.U4)]
            public int cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public uint dwTime;
        }

        #endregion

        #region Bindable

        /// <summary>
        /// 信息窗内容;
        /// </summary>
        public string Info { get; set; }

        #endregion

        #region Command

        public ICommand StartCommand { get; set; }

        public ICommand StopCommand { get; set; }

        #endregion

        public GetLastInputInfoViewModel()
        {
            SetCommandMethod();
            InitValue();
            Console.SetOut(new ConsoleWriter(ShowInfo));

            _timer.Elapsed += Timer_Elapsed;
        }

        /// <summary>
        /// 定时器执行方法
        /// </summary>
        private void Timer_Elapsed(object sender, EventArgs e)
        {
            long passedMillisecond = GetIdleTick();
            ShowInfo($"鼠标/键盘/触摸屏 已经没有操作 {passedMillisecond} 毫秒（{passedMillisecond / 1000} 秒 / {passedMillisecond / 1000.0 / 60} 分钟）");

            //if (passedMillisecond / 1000 > 10)
            //{
            //    //MessageBox.Show("键盘或鼠标没有操作超过 10 秒");
            //    ShowInfo("键盘或鼠标没有操作超过 10 秒");
            //}
        }

        /// <summary>
        /// 获取空闲的毫秒数
        /// </summary>
        /// <returns></returns>
        public static long GetIdleTick()
        {
            LASTINPUTINFO lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = Marshal.SizeOf(lastInputInfo);
            if (!GetLastInputInfo(ref lastInputInfo)) return 0;
            return Environment.TickCount - (long)lastInputInfo.dwTime;
        }

        private void InitValue()
        {
            ShowInfo($"通过调用 Windows API 中的 GetLastInputInfo 来获取最后一次输入的时间。检测间隔：{_timer.Interval}毫秒。");
        }

        /// <summary>
        /// 命令方法赋值(在构造函数中调用)
        /// </summary>
        private void SetCommandMethod()
        {
            StartCommand ??= new RelayCommand(o => true, o =>
            {
                _timer.Start();
            });

            StopCommand ??= new RelayCommand(o => true, o =>
            {
                _timer.Stop();
            });
        }

        #region 辅助方法

        private void ShowInfo(string info)
        {
            Info += $"{info}\r\n\r\n";
        }

        #endregion
    }
}
