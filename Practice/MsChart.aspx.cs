﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

namespace Practice
{
    /// <summary>
    /// ASP.NET 微软图表控件 MsChart
    /// 参考：http://www.cnblogs.com/suguoqiang/archive/2013/01/16/2862945.html
    /// </summary>
    public partial class MsChartWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = CreatData();

            #region 折线图
            Chart1.DataSource = dt;//绑定数据
            Chart1.Series["Series1"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;//设置图表类型
            Chart1.Series[0].XValueMember = "Language";//X轴数据成员列
            Chart1.Series[0].YValueMembers = "Count";//Y轴数据成员列
            Chart1.ChartAreas["ChartArea1"].AxisX.Title = "语言";//X轴标题
            Chart1.ChartAreas["ChartArea1"].AxisX.TitleAlignment = StringAlignment.Far;//设置Y轴标题的名称所在位置位远
            Chart1.ChartAreas["ChartArea1"].AxisY.Title = "统计";//X轴标题
            Chart1.ChartAreas["ChartArea1"].AxisY.TitleAlignment = StringAlignment.Far;//设置Y轴标题的名称所在位置位远
            Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;//X轴数据的间距
            Chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = false;//不显示竖着的分割线
            Chart1.Series[0].IsValueShownAsLabel = true;//显示坐标值
            #endregion

            #region 样条图（平滑曲线）
            Chart2.DataSource = dt;//绑定数据
            Chart2.Series["Series1"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Spline;//设置图表类型
            Chart2.Series["Series1"].MarkerStyle = System.Web.UI.DataVisualization.Charting.MarkerStyle.Cross;//设置点的样式，十字形
            Chart2.Series[0].XValueMember = "Language";//X轴数据成员列
            Chart2.Series[0].YValueMembers = "Count";//Y轴数据成员列
            Chart2.ChartAreas["ChartArea1"].AxisX.Title = "语言";//X轴标题
            Chart2.ChartAreas["ChartArea1"].AxisX.TitleAlignment = StringAlignment.Far;//设置Y轴标题的名称所在位置位远
            Chart2.ChartAreas["ChartArea1"].AxisY.Title = "统计";//X轴标题
            Chart2.ChartAreas["ChartArea1"].AxisY.TitleAlignment = StringAlignment.Far;//设置Y轴标题的名称所在位置位远
            Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;//X轴数据的间距
            Chart2.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = false;//不显示竖着的分割线
            Chart2.Series[0].IsValueShownAsLabel = true;//显示坐标值
            #endregion

            #region 条形图
            Chart3.DataSource = dt;//绑定数据
            Chart3.Series["Series1"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Bar;//设置图表类型
            Chart3.Series[0].XValueMember = "Language";//X轴数据成员列
            Chart3.Series[0].YValueMembers = "Count";//Y轴数据成员列
            Chart3.ChartAreas["ChartArea1"].AxisX.Title = "语言";//X轴标题
            Chart3.ChartAreas["ChartArea1"].AxisX.TitleAlignment = StringAlignment.Far;//设置Y轴标题的名称所在位置位远
            Chart3.ChartAreas["ChartArea1"].AxisY.Title = "统计";//X轴标题
            Chart3.ChartAreas["ChartArea1"].AxisY.TitleAlignment = StringAlignment.Far;//设置Y轴标题的名称所在位置位远
            Chart3.ChartAreas["ChartArea1"].AxisX.Interval = 1;//X轴数据的间距
            Chart3.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = false;//不显示竖着的分割线
            Chart3.Series[0].IsValueShownAsLabel = true;//显示坐标值           
            #endregion

            #region 饼形图
            Chart4.DataSource = dt;//绑定数据
            Chart4.Series["Series1"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Pie;//设置图表类型
            Chart4.Series[0].XValueMember = "Language";//X轴数据成员列
            Chart4.Series[0].YValueMembers = "Count";//Y轴数据成员列
            //Chart4.Series[0].LegendText = "Language"; //是自动设置的;            
            Chart4.Series[0].IsValueShownAsLabel = true;//显示坐标值

            #endregion

            //System.Web.UI.DataVisualization.Charting.SeriesChartType.Radar

            #region 点状图和雷达图
            Chart5.DataSource = dt;//绑定数据
            
            Chart5.Series[0].XValueMember = "Language";//X轴数据成员列
            Chart5.Series[0].YValueMembers = "Count";//Y轴数据成员列                      
            Chart5.Series[0].IsValueShownAsLabel = true;//显示坐标值
            Chart5.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = false;//不显示竖着的分割线
            //Chart5.ChartAreas["ChartArea1"].AxisY.MajorGrid.Enabled = false;//不显示横着的分割线

            Chart5.Series["Series2"].XValueMember = "Language";//X轴数据成员列
            Chart5.Series["Series2"].YValueMembers = "Count";//Y轴数据成员列

            #endregion
        }

        /// <summary>
        /// 创建一张二维数据表
        /// </summary>
        /// <returns>Datatable类型的数据表</returns>
        DataTable CreatData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Language", System.Type.GetType("System.String"));
            dt.Columns.Add("Count", System.Type.GetType("System.String"));

            string[] n = new string[] { "C#", "Java", "Object-C" };
            string[] c = new string[] { "30", "50", "35" };

            for (int i = 0; i < 3; i++)
            {           
                DataRow dr = dt.NewRow();
                dr["Language"] = n[i];
                dr["Count"] = c[i];
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}