﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Spire.Pdf;
using Spire.Pdf.Bookmarks;
using Spire.Pdf.General;

namespace WinFormPractice
{
    /// <summary>
    /// 测试：《C# 添加、修改和删除 PDF 书签》
    /// https://www.cnblogs.com/Yesi/p/7251214.html
    /// </summary>
    public partial class FormSpirePdf : Form
    {
        private string _FileName = "AddBookmark.pdf";

        public FormSpirePdf()
        {
            InitializeComponent();
        }

        private void BtnAddBookmarks_Click(object sender, EventArgs e)
        {
            //新建PDF文档
            PdfDocument pdf = new PdfDocument();

            //添加页面
            PdfPageBase page = pdf.Pages.Add();

            //添加书签
            PdfBookmark bookmark = pdf.Bookmarks.Add("第一页");

            //设置书签所指向的页面和位置，（0,0）表示页面的开始位置
            bookmark.Destination = new PdfDestination(page)
            {
                Location = new PointF(0, 0)
            };

            //设置书签的文本格式和颜色
            bookmark.DisplayStyle = PdfTextStyle.Bold;
            bookmark.Color = Color.Black;

            //再添加一页，供其它功能测试使用;
            pdf.Pages.Add();

            //保存文档
            pdf.SaveToFile(_FileName);

            MessageBox.Show($"已保存文件{_FileName}");
        }

        private void BtnAddSubBookmarks_Click(object sender, EventArgs e)
        {
            //新建PDF文档
            PdfDocument pdf = new PdfDocument();

            //添加页面
            PdfPageBase page = pdf.Pages.Add();

            //添加书签
            PdfBookmark bookmark = pdf.Bookmarks.Add("第一章 热传导");

            //设置书签指向的页面和位置
            bookmark.Destination = new PdfDestination(page) {Location = new PointF(0, 0)};

            //设置书签的文本格式和颜色
            bookmark.DisplayStyle = PdfTextStyle.Bold;
            bookmark.Color = Color.SeaGreen;

            //添加子书签
            PdfBookmark childBookmark = bookmark.Insert(0, "1.1 热传导基本知识");

            //设置子书签指向的页面和位置
            childBookmark.Destination = new PdfDestination(page) {Location = new PointF(400, 300)};

            //设置子书签的文本格式和颜色
            childBookmark.DisplayStyle = PdfTextStyle.Regular;
            childBookmark.Color = Color.Black;

            //保存文档
            string fileName = "ChildBookmark.pdf";
            pdf.SaveToFile(fileName);

            MessageBox.Show($"已保存文件{fileName}");
        }

        private void BtnAddToFile_Click(object sender, EventArgs e)
        {
            //加载文档
            PdfDocument pdf = new PdfDocument();
            pdf.LoadFromFile(_FileName);

            for (int i = 0; i < pdf.Pages.Count; i++)
            {
                //添加书签
                PdfBookmark bookmark = pdf.Bookmarks.Add($"第{i + 1}章");

                //设置书签指向的页面和位置
                bookmark.Destination = new PdfDestination(pdf.Pages[i]) {Location = new PointF(0, 0)};

                //设置书签的文本格式和颜色
                bookmark.DisplayStyle = PdfTextStyle.Bold;
                bookmark.Color = Color.Black;
            }

            //保存文档
            pdf.SaveToFile(_FileName);

            MessageBox.Show($"已保存文件{_FileName}");
        }

        private void BtnModify_Click(object sender, EventArgs e)
        {
            //加载文档
            PdfDocument pdf = new PdfDocument();
            pdf.LoadFromFile(_FileName);

            //获取书签列表
            PdfBookmarkCollection bookmarks = pdf.Bookmarks;

            //获取第一个书签
            PdfBookmark bookmark = bookmarks[0];

            //修改书签指向的页面
            bookmark.Destination = new PdfDestination(pdf.Pages[1]);

            //修改书签的文本格式和颜色
            bookmark.DisplayStyle = PdfTextStyle.Bold;
            bookmark.Color = Color.Green;

            //修改书签的title
            bookmark.Title = "修改";

            //保存文档
            string fileName = "ModifyBookmark.pdf";
            pdf.SaveToFile(fileName);

            MessageBox.Show($"已保存文件{fileName}");
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            //加载文档
            PdfDocument pdf = new PdfDocument();
            pdf.LoadFromFile(_FileName);

            //插入新书签到指定位置（此处插入的是第三个书签的位置）
            PdfBookmark bookmark = pdf.Bookmarks.Insert(2, "新增第三章");

            //设置书签所指向的页面和位置
            bookmark.Destination = new PdfDestination(pdf.Pages[1]);
            bookmark.Destination.Location = new PointF(0, 300);

            //保存文档
            string fileName = "InsertBookmark.pdf";
            pdf.SaveToFile(fileName);

            MessageBox.Show($"已保存文件{fileName}");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            //加载文档
            PdfDocument pdf = new PdfDocument();
            pdf.LoadFromFile(_FileName);

            //获取书签列表
            PdfBookmarkCollection bookmarks = pdf.Bookmarks;

            //删除第一个书签
            bookmarks.RemoveAt(0);

            //保存文档
            string fileName = "DeleteBookmark.pdf";
            pdf.SaveToFile(fileName);

            MessageBox.Show($"已保存文件{fileName}");
        }

        /// <summary>
        /// 破解方法：移除第一页版权信息
        /// https://blog.csdn.net/weixin_38211198/article/details/90115310
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCrack_Click(object sender, EventArgs e)
        {
            //新建PDF文档
            PdfDocument pdf = new PdfDocument();

            //添加页面
            pdf.Pages.Add();
            pdf.Pages.Add();
            pdf.Pages.RemoveAt(0);

            //保存文档
            string fileName = "Crack.pdf";
            pdf.SaveToFile(fileName);

            MessageBox.Show($"已保存文件{fileName}");
        }

        private void BtnChooseFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                TBPath.Text = _FileName = openFileDialog.FileName;
                MessageBox.Show($"已选择文件{_FileName}");

                if (LoadBookmarks())
                {
                    MessageBox.Show("载入书签成功");
                }
                else
                {
                    MessageBox.Show("载入书签失败");
                }
            }
        }

        private bool LoadBookmarks()
        {
            bool result = false;

            try
            {
                PdfDocument pdf = new PdfDocument();
                pdf.LoadFromFile(_FileName);

                //获取书签列表
                PdfBookmarkCollection bookmarks = pdf.Bookmarks;

                //TVBookmarks.

                result = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return result;
        }
    }
}
