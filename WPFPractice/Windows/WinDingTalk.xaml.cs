﻿using DotNet.Utilities.ConsoleHelper;
using DotNet.Utilities.DingTalk;
using DotNet.Utilities.WpfHelpers;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using WPFPractice.Properties;

namespace WPFPractice.Windows
{
    public partial class WinDingTalk : Window
    {
        public WinDingTalk()
        {
            InitializeComponent();

            this.DataContext = new DingTalkViewModel();
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class DingTalkViewModel : BindableBase
    {
        #region 成员

        #endregion

        #region Bindable

        public string Url { get; set; } = "192.168.16.120";

        public string Keyword { get; set; } = ".NET通知";

        public string UserName { get; set; }

        public string Password { get; set; }
        
        public string UserId { get; set; }

        public string Mobile { get; set; }

        /// <summary>
        /// 发送的标题;
        /// </summary>
        public string ChatTitle { get; set; } = "标题";

        /// <summary>
        /// 发送的内容;
        /// </summary>
        public string ChatContent { get; set; }

        /// <summary>
        /// 信息窗内容;
        /// </summary>
        public string Info { get; set; }

        #endregion

        #region Command

        public ICommand SaveUrlCommand { get; set; }

        public ICommand LoginCommand { get; set; }

        public ICommand AddFriendCommand { get; set; }

        public ICommand SendMessageCommand { get; set; }

        #endregion

        public DingTalkViewModel()
        {
            SetCommandMethod();

            InitValue();

            Console.SetOut(new ConsoleWriter(ShowInfo));
        }

        private void InitValue()
        {
            if (!string.IsNullOrWhiteSpace(Settings.Default.Url))
            {
                Url = Settings.Default.Url;
            }

            if (!string.IsNullOrWhiteSpace(Settings.Default.Keyword))
            {
                Keyword = Settings.Default.Keyword;
            }

            if (!string.IsNullOrWhiteSpace(Settings.Default.UserName))
            {
                UserName = Settings.Default.UserName;
            }

            if (!string.IsNullOrWhiteSpace(Settings.Default.Password))
            {
                Password = Settings.Default.Password;
            }

            ShowInfo("测试钉钉群自定义机器人\r\n官方文档：https://ding-doc.dingtalk.com/doc#/serverapi2/qf2nxq");
        }

        /// <summary>
        /// 命令方法赋值(在构造函数中调用)
        /// </summary>
        private void SetCommandMethod()
        {
            SaveUrlCommand ??= new RelayCommand(o => !string.IsNullOrEmpty(Url), o =>
            {
                Settings.Default.Url = Url;
                Settings.Default.Keyword = Keyword;
                Settings.Default.Save();
                ShowInfo("操作成功");
            });

            LoginCommand ??= new RelayCommand(o => !(string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password)), async o =>
            {
                Settings.Default.UserName = UserName;
                Settings.Default.Password = Password;
                Settings.Default.Save();
            });

            AddFriendCommand ??= new RelayCommand(o => false, o =>
            {
                
            });

            SendMessageCommand ??= new RelayCommand(o => !(string.IsNullOrEmpty(ChatTitle) || string.IsNullOrEmpty(ChatContent)), o =>
            {
                DingTalkRobotHelper.Config(Url, new List<string>() { Mobile });
                DingTalkRobotHelper.Send(ChatContent, ChatTitle);
                //DingTalkRobotHelper.Send(ChatContent, ChatTitle, RobotSendMsgType.markdown);
            });
        }

        #region 辅助方法

        private void ShowInfo(string info)
        {
            Info += $"{info}\r\n\r\n";
        }

        #endregion
    }
}
