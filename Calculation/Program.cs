﻿using System;

namespace Calculation
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Console.WriteLine("Calculation:");
                //for (int i = 0; i <= 4; i++)
                //{
                //    Console.WriteLine(Calculation.GetResult(i, 1));
                //    Console.WriteLine(Calculation.GetResult(i, 1000000000000000000));
                //    Console.WriteLine(Calculation.GetResult(i, 3.5F));
                //    Console.WriteLine(Calculation.GetResult(i, 4.2M));
                //}

                //Console.WriteLine("Calculation2:\r\n");
                //for (int i = 0; i <= 4; i++)
                //{
                //    Console.WriteLine(Calculation2.GetResult(i, 1));
                //    Console.WriteLine(Calculation2.GetResult(i, 1000000000000000000));
                //    Console.WriteLine(Calculation2.GetResult(i, 3.5F));
                //    Console.WriteLine(Calculation2.GetResult(i, 4.2M));
                //    Console.WriteLine();
                //}

                Console.WriteLine("Calculation<T>:\r\n");
                for (int i = 0; i <= 4; i++)
                {
                    Console.WriteLine(Calculation3.GetResult(i, 1));
                    Console.WriteLine(Calculation3.GetResult(i, 1000000000000000000L));
                    Console.WriteLine(Calculation3.GetResult(i, 3.5F));
                    Console.WriteLine(Calculation3.GetResult(i, 4.2M));
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
