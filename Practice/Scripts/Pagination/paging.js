﻿/* ------------------------------- 分页配置 Star ------------------------------ */
var paging = function (obj, curPage, totalDataCount, perPageNum, func) {
    // 分页插件初始化
    obj.pagination({
        coping: true,                       // 是否开启首页和末页
        keepShowPN: true,                   // 是否一直显示上一页下一页
        jump: true,                         // 跳转功能
        isHide: true,                       // 当页数为0或1是隐藏
        homePage: '首页',                   // 首页文字
        endPage: '末页',                    // 末页文字
        totalData: totalDataCount,          // 总数据
        showData: perPageNum,               // 每页数据条数
        current: curPage,                   // 当前页数
        prevContent: '上一页',              // 上一页文字内容
        nextContent: '下一页',              // 下一页文字内容
        prevCls: "page-prev",               // 上一页类名
        nextCls: "page-next",               // 下一页类名
        activeCls: "on",                    // 当前类名
        callback: function (api) {          // 回调
            func(api.getCurrent());
        }
    });
}
/* ------------------------------- 分页配置 End ------------------------------ */