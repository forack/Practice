﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="jobSeek_List.aspx.cs" Inherits="jobSeek_List" %>

<%@ Register Src="~/Controller/Header.ascx" TagPrefix="Header" TagName="Header" %>
<%@ Register Src="~/Controller/Right.ascx" TagPrefix="Right" TagName="Right" %>
<%@ Register Src="~/Controller/Bottom.ascx" TagPrefix="Bottom" TagName="Bottom" %>
<%@ Register Src="~/Controller/MemberLeft.ascx" TagPrefix="Left" TagName="Left" %>

<!doctype html>
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta http-equiv="content-type" charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=1200,initial-scale=0.3, minimum-scale=0.3, maximum-scale=1.0, user-scalable=no">
    <meta name="designer" content="网站开发设计制作：杭州蒙特信息技术有限公司 www.mountor.cn 咨询电话0571-88368188" />
    <meta name="description" content="<%=strSeoDescription %>" />
    <meta name="keywords" content="<%=strSeoKeywords %>" />
    <title><%=strSeoTitle %></title>
    <link href="css/rest.css" rel="stylesheet" type="text/css">
    <link href="css/module.css" rel="stylesheet" type="text/css">
    <link href="css/jQValidator.css" rel="stylesheet" type="text/css">
    <link href="css/member.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript" src="js/jquery.pagination.js"></script>
    <!--[if IE 6]> 
<script src="js/iepng.js" type="text/javascript"></script> 
<script type="text/javascript">
   EvPNG.fix('div, ul, img, li, input, a, span ,p ,dt ,dd');  
</script>
<![endif]-->
    <!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/placeholder.js"></script>
<![endif]-->
</head>

<body>

    <div class="wrap">
        <!---- 头部开始 ---->
        <Header:Header runat="server" />
        <!---- 头部结束 ---->

        <!-- 主要内容框开始 -->
        <div class="main-box">
            <div class="page-member-box page-outMana-box">
                <div class="content">
                    <!--- 左侧部分开始 --->
                    <Left:Left runat="server" />
                    <!--- 左侧部分结束 --->

                    <!--- 右侧主体部分开始 --->
                    <div class="global-member-main">
                        <!-- global-member-hd start -->
                        <div class="global-member-hd">
                            <h2 class="global-title">求活信息管理</h2>

                            <div class="tool-box">
                                <a href="<%=strDetailUrl %>" class="btn btn-add">+新增求活信息</a>
                            </div>
                        </div>
                        <!-- global-member-hd end -->

                        <!-- out-mana-table start -->
                        <div class="out-mana-table live">
                            <table cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr class="tt">
                                        <th class="name">标题</th>
                                        <th class="type">空闲情况</th>
                                        <th class="area">区域</th>
                                        <th class="date">发布日期</th>
                                        <th class="operate">&nbsp;</th>
                                    </tr>
                                </thead>

                                <tbody id="list">
                                   
                                </tbody>
                            </table>
                        </div>
                        <!-- out-mana-table end -->

                        <!--分页开始-->
                        <div class="page">
                        </div>
                        <!--分页结束-->
                    </div>
                    <!--- 右侧主体部分结束 --->
                </div>
            </div>
        </div>
        <!-- 主要内容框结束 -->

        <!----右侧悬浮开始---->
        <Right:Right runat="server" />
        <!----右侧悬浮结束---->

        <!-- 底部开始 -->
        <Bottom:Bottom runat="server" />
        <!-- 底部结束 -->

        <!-- 确定是否删除开始 -->
        <div class="pop-box pop-member-ts delete">
            <div class="pop-inner">
                <a href="javascript:void(0)" class="pop-close"></a>

                <div class="pop-bd">
                    <img src="images/frm_02.png" class="icon-ts" />
                    <div class="doc">确定是否删除？</div>
                    <div class="btn-tool">
                        <a href="javascript:void(0)" class="btn btn-blue btn-sure">确认删除</a>
                        <a href="javascript:void(0)" class="btn pop-cancel">取消</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- 确定是否删除结束 -->

    </div>

</body>

</html>
<script type="text/javascript" src="http://v2.jiathis.com/code/jia.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jQValidator.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/member.js"></script>
<script type="text/javascript" src="js/paging.js"></script>
<script type="text/javascript">
    var showData=<%=pageCount%>;            // 每页展示数
    var kind="<%=curKind%>";                // 当前栏目
    var detailUrl="<%=strDetailUrl%>";      // 详情页面
    var curPage=1;
    
    // 初始化加载
    $(function () {
        loadData(1);
    });
    // 加载页面
    function loadData(page) {
        curPage=page;
        $.ajax({
            type: "post",
            dataType: "json",
            url: "/ashx/project/getProjects.ashx",
            data: {
                kind:kind,
                page:page,
                pageCount:showData
            },
            success: function (data) {
                $("#list").empty();             // 清空列表
                if (data.msg == "1") {
                    var rows=data.data;
                    if(rows&&rows.length>0){
                        for (var i = 0; i < rows.length; i++) {
                            var row=rows[i];
                            var html="<tr>";
                            html+="<td class='name'><a href='"+detailUrl+"&id="+row.id+"'>"+row.c_info_title+"</a></td>";
                            html+="<td class='type'>"+row.a8+"</td>";
                            html+="<td class='area'>"+row.a5+"</td>";
                            html+="<td class='date'>"+row.publishTime+"</td>";
                            html+="<td class='operate'><a href='javascript:void(0)' data-id='"+row.id+"' class='btn btn-mana-del'>删除</a></td>";
                            html+="</tr>";
                            $("#list").append(html);
                            paging($(".page-paging"),curPage,data.total,showData,loadData)
                        }
                        bindEvent();
                    }else {
                    $("#list").append("<p style='line-height: 50px;'>未查询到相应数据信息......</p>");
                    }
                } else {
                    alert(data.msg);
                }
            },
            error: function () {
            }
        });
    }

    var curID="0";
    // 绑定事件
    function bindEvent() {
        $(".btn-mana-del").click(function () {
            curID=$(this).attr("data-id");
            popshow('.pop-box.pop-member-ts.delete');
        });
    }

    $(function () {
        loadData(curPage);
    });

    var delStatu=false;
    // 确认删除
    $(".btn-sure").click(function () {
        if (delStatu) {
            return;
        }
        delStatu = true;
        $('.pop-box.pop-member-ts.delete').fadeOut();
        $.ajax({
            type: "post",
            dataType: "text",
            url: "/ashx/project/deleteProject.ashx",
            data: {id:curID},
            success: function (data) {
                delStatu = false;
                if (data == "1") {
                    loadData(curPage);
                } else {
                    alert("删除失败");
                }
            },
            error: function () {
                delStatu = false;
                alert("删除失败");
            }
        });
    });
</script>