﻿using System;
using System.Collections.Generic;
using DingTalk.Api;
using DingTalk.Api.Request;
using DingTalk.Api.Response;
/*
 * 代码已托管 https://gitee.com/dlgcy/dotnetcodes/tree/dlgcy/DotNet.Utilities/DingTalk
 */
namespace DotNet.Utilities.DingTalk
{
    /// <summary>
    /// [dlgcy]钉钉群机器人帮助类
    /// 官方文档：https://ding-doc.dingtalk.com/doc#/serverapi2/qf2nxq
    /// </summary>
    public class DingTalkRobotHelper
    {
        /// <summary>
        /// 对应于某个群的链接地址
        /// </summary>
        private static string Url { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        private static string Keyword { get; set; }

        /// <summary>
        /// 需要通知的人员的手机号(对应于钉钉群中的人员)列表;
        /// </summary>
        private static List<string> MobileList { get; set; } = new List<string>();

        /// <summary>
        /// 配置参数
        /// </summary>
        /// <param name="url">对应于某个群的链接地址</param>
        /// <param name="mobileList">需要通知的人员的手机号(对应于钉钉群中的人员)列表</param>
        /// <param name="keyword">关键字(申请链接时设置的)</param>
        public static void Config(string url, List<string> mobileList, string keyword = ".NET通知")
        {
            if (!string.IsNullOrEmpty(url))
            {
                Url = url;
            }

            if (mobileList != null)
            {
                MobileList = mobileList;
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                Keyword = keyword;
            }
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="msgContent">内容</param>
        /// <param name="magTitle">标题</param>
        /// <param name="msgType">发送格式</param>
        /// <returns></returns>
        public static bool Send(string msgContent, string magTitle = "（无标题）", RobotSendMsgType msgType = RobotSendMsgType.text)
        {
            if (string.IsNullOrEmpty(Url))
            {
                Console.WriteLine($"发送地址为空，请在程序初始化阶段先使用 {nameof(Config)} 方法对参数进行配置。");
            }

            IDingTalkClient dingTalk = new DefaultDingTalkClient(Url);

            OapiRobotSendRequest request = new OapiRobotSendRequest()
            {
                At_ = new OapiRobotSendRequest.AtDomain()
                {
                    AtMobiles = MobileList,
                    IsAtAll = false
                },

                Msgtype = msgType.ToString(),
            };

            switch (msgType)
            {
                default:
                case RobotSendMsgType.text:
                {
                    request.Text_ = new OapiRobotSendRequest.TextDomain()
                    {
                        Content = $"{Keyword}：【{magTitle}】 \n{msgContent.Replace("\r\n", "\n")}"
                    };
                    break;
                }
                case RobotSendMsgType.markdown:
                {
                    request.Markdown_ = new OapiRobotSendRequest.MarkdownDomain()
                    {
                        Title = $"{Keyword}",
                        Text = $"### {magTitle} \n> {msgContent.Replace("\r\n", "\n> ")}", //换行未成功
                    };
                    break;
                }
            }

            OapiRobotSendResponse response = dingTalk.Execute(request);

            if (response.IsError)
            {
                Console.WriteLine($"发送钉钉消息出错：{response.Errcode}-{response.Errmsg}");
                return false;
            }
            else
            {
                Console.WriteLine($"发送钉钉消息成功");
                return true;
            }
        }
    }

    /// <summary>
    /// 钉钉群机器人发送消息类型枚举
    /// </summary>
    public enum RobotSendMsgType
    {
        text,
        link,
        markdown,
        actionCard,
        feedCard
    }
}
