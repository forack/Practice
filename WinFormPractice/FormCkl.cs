﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WinFormPractice
{
    public partial class FormCkl : Form
    {
        public FormCkl()
        {
            InitializeComponent();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (ckl1.CheckedItems.Contains("1") && ckl1.CheckedItems.Contains("3"))
            {
                MessageBox.Show("OK");
            }
            else
            {
                MessageBox.Show("不OK");
            }
        }

        /// <summary>
        /// dlgcy:获取CheckedListBox选中项List;
        /// </summary>
        private List<T> GetCheckedListBoxCheckedList<T>(CheckedListBox ckl)
        {
            List<T> list = new List<T>();
            foreach (var item in ckl.CheckedItems)
            {
                list.Add((T)item);
            }

            return list;
        }

        private void btnCheck2_Click(object sender, EventArgs e)
        {
            List<string> checkedList = GetCheckedListBoxCheckedList<string>(ckl1);
            List<string> compareList = new List<string>(){"1", "3"};

            if(checkedList.All(compareList.Contains) && checkedList.Count == compareList.Count)
            {
                MessageBox.Show("OK");
            }
            else
            {
                MessageBox.Show("不OK");
            }
        }
    }
}
