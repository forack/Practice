﻿using DotNet.Utilities.WpfHelpers;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PropertyChanged;
using unvell.ReoGrid;
using unvell.ReoGrid.IO;

namespace WPFPractice.Windows
{
    /// <summary>
    /// ReoGrid 表格控件使用示例
    /// </summary>
    public partial class WinReoGrid : Window
    {
        private ReoGridViewModel _VM;

        public WinReoGrid()
        {
            InitializeComponent();
            _VM = new ReoGridViewModel();
            DataContext = _VM;

            Task.Run(() =>
            {
                LoadData();
                SetReoGridControl();
                //ShowAndHideColumns();
            });
        }

        /// <summary>
        /// 载入数据
        /// </summary>
        private void LoadData()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                var workbook = reoGridControl;
                try
                {
                    using (Stream stream = Application.GetResourceStream(new Uri("/WPFPractice;component/Docs/合并中英文对照并换行显示.xlsx", UriKind.Relative)).Stream)
                    {
                        workbook.Load(stream, FileFormat.Excel2007);
                    }
                }
                catch (Exception ex)
                {
                    _VM.ShowInfo($"载入用户权限表异常：{ex}");
                }
            }));
        }

        /// <summary>
        /// 设置控件
        /// </summary>
        private void SetReoGridControl()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                //滚动条设置;
                //var workbookSettingsDisable = WorkbookSettings.View_ShowScrolls;
                //reoGridControl.DisableSettings(workbookSettingsDisable);

                var worksheet = reoGridControl.CurrentWorksheet;
                worksheet.SelectionStyle = WorksheetSelectionStyle.None; //选择范围样式;

                //冻结行和列;
                worksheet.FreezeToCell(2, 1, FreezeArea.LeftTop);

                //禁用显示行和列头;
                var worksheetSettingsDisable = WorksheetSettings.View_ShowRowHeader | WorksheetSettings.View_ShowColumnHeader;
                worksheet.DisableSettings(worksheetSettingsDisable);

                //设置只读;
                var worksheetSettingsEnable = WorksheetSettings.Edit_Readonly;
                worksheet.EnableSettings(worksheetSettingsEnable);

                //设置显示的行数和列数;
                worksheet.SetCols(3);
                worksheet.SetRows(13);
            }));
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class ReoGridViewModel : BindableBase
    {
        #region 成员

        /// <summary>
        /// 状态信息
        /// </summary>
        public string StatusInfo { get; set; }

        #endregion

        public ReoGridViewModel()
        {
            SetCommandMethod();
        }

        #region 辅助方法

        public void ShowInfo(string info)
        {
            StatusInfo = $"{info}";
        }

        #endregion

        #region 命令

        public ICommand AddBtnCommand { get; set; }

        public ICommand DelBtnCommand { get; set; }

        #endregion

        /// <summary>
        /// 命令方法赋值（在构造方法中调用）
        /// </summary>
        private void SetCommandMethod()
        {
            AddBtnCommand ??= new RelayCommand(o => true, async o =>
            {

            });

            DelBtnCommand ??= new RelayCommand(o => true, async o =>
            {

            });
        }
    }
}
