﻿
namespace WinFormPractice
{
    partial class FormProgressDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_Total = new System.Windows.Forms.Label();
            this.progressBar = new CCWin.SkinControl.SkinProgressBar();
            this.rtb_Title = new System.Windows.Forms.RichTextBox();
            this.lbl_Current = new System.Windows.Forms.Label();
            this.btn_Abort = new CCWin.SkinControl.SkinButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lbl_Total, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.progressBar, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.rtb_Title, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_Current, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(42, 50);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(716, 225);
            this.tableLayoutPanel1.TabIndex = 28;
            // 
            // lbl_Total
            // 
            this.lbl_Total.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_Total.AutoSize = true;
            this.lbl_Total.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_Total.ForeColor = System.Drawing.Color.Gray;
            this.lbl_Total.Location = new System.Drawing.Point(277, 74);
            this.lbl_Total.Name = "lbl_Total";
            this.lbl_Total.Size = new System.Drawing.Size(162, 22);
            this.lbl_Total.TabIndex = 25;
            this.lbl_Total.Text = "共 5 项，已执行 3 项";
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar.Back = null;
            this.progressBar.BackColor = System.Drawing.Color.Transparent;
            this.progressBar.BarBack = null;
            this.progressBar.BarRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.progressBar.ForeColor = System.Drawing.Color.White;
            this.progressBar.Location = new System.Drawing.Point(147, 125);
            this.progressBar.Name = "progressBar";
            this.progressBar.Radius = 10;
            this.progressBar.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.progressBar.Size = new System.Drawing.Size(422, 23);
            this.progressBar.TabIndex = 23;
            this.progressBar.TrackBack = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            // 
            // rtb_Title
            // 
            this.rtb_Title.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtb_Title.BackColor = System.Drawing.Color.White;
            this.rtb_Title.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_Title.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtb_Title.Font = new System.Drawing.Font("微软雅黑 Light", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rtb_Title.Location = new System.Drawing.Point(162, 19);
            this.rtb_Title.Multiline = false;
            this.rtb_Title.Name = "rtb_Title";
            this.rtb_Title.ReadOnly = true;
            this.rtb_Title.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtb_Title.Size = new System.Drawing.Size(391, 28);
            this.rtb_Title.TabIndex = 24;
            this.rtb_Title.TabStop = false;
            this.rtb_Title.Text = "业务1 任务批量执行中...";
            // 
            // lbl_Current
            // 
            this.lbl_Current.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbl_Current.AutoSize = true;
            this.lbl_Current.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_Current.ForeColor = System.Drawing.Color.Gray;
            this.lbl_Current.Location = new System.Drawing.Point(258, 186);
            this.lbl_Current.Name = "lbl_Current";
            this.lbl_Current.Size = new System.Drawing.Size(200, 22);
            this.lbl_Current.TabIndex = 26;
            this.lbl_Current.Text = "当前正在执行：001号任务";
            // 
            // btn_Abort
            // 
            this.btn_Abort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_Abort.BackColor = System.Drawing.Color.Transparent;
            this.btn_Abort.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Abort.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Abort.DownBack = null;
            this.btn_Abort.Font = new System.Drawing.Font("微软雅黑", 14.25F);
            this.btn_Abort.Location = new System.Drawing.Point(291, 32);
            this.btn_Abort.MouseBack = null;
            this.btn_Abort.Name = "btn_Abort";
            this.btn_Abort.NormlBack = null;
            this.btn_Abort.Radius = 10;
            this.btn_Abort.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.btn_Abort.Size = new System.Drawing.Size(134, 35);
            this.btn_Abort.TabIndex = 0;
            this.btn_Abort.Text = "中止";
            this.btn_Abort.UseVisualStyleBackColor = false;
            this.btn_Abort.Click += new System.EventHandler(this.btn_Abort_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btn_Abort, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(42, 301);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(716, 100);
            this.tableLayoutPanel2.TabIndex = 29;
            // 
            // FormProgressDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaptionFont = new System.Drawing.Font("Microsoft YaHei UI Light", 14.25F, System.Drawing.FontStyle.Bold);
            this.CaptionHeight = 35;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "FormProgressDialog";
            this.Radius = 10;
            this.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.Shadow = true;
            this.ShowBorder = false;
            this.ShowDrawIcon = false;
            this.ShowIcon = false;
            this.Special = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "正在处理中...";
            this.TitleCenter = false;
            this.TitleOffset = new System.Drawing.Point(10, 0);
            this.TitleSuitColor = true;
            this.Load += new System.EventHandler(this.FormProgressDialog_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lbl_Total;
        private CCWin.SkinControl.SkinProgressBar progressBar;
        private System.Windows.Forms.RichTextBox rtb_Title;
        private System.Windows.Forms.Label lbl_Current;
        private CCWin.SkinControl.SkinButton btn_Abort;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}