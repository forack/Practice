﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AspNetPager_Test.aspx.cs" Inherits="Practice.AspNetPager_Test" %>
<%@ Register TagPrefix="webdiyer" Namespace="Wuqi.Webdiyer" Assembly="AspNetPager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Repeater ID="rep" runat="server">
        <HeaderTemplate>
            <table width="100%" class="table table-bordered table-striped table-hover">
            <tr><th style="width:15%">订单编号</th><th style="width:30%">公司名称</th><th style="width:20%">客户编号</th><th style="width:20%">雇员姓名</th></tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("orderid")%></td>               
                <td><%# Eval("companyname")%></td>
                <td><%# Eval("customerid")%></td>
                <td><%# Eval("employeename")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    
    <div>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="true" CssClass="" LayoutType="Ul" 
            PagingButtonLayoutType="Span" PagingButtonSpacing="10" CurrentPageButtonClass="active" PageSize="10" OnPageChanged="AspNetPager1_OnPageChanged">
        </webdiyer:AspNetPager>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FootContent" runat="server">
</asp:Content>
