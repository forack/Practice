﻿using CCWin;
using DotNet.Utilities.WinFormHelper;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormPractice
{
    public partial class FormProgressDialog : Skin_DevExpress
    {
        /// <summary>
        /// 执行操作事件
        /// </summary>
        public event Action OperateAction;

        /// <summary>
        /// 终止操作事件
        /// </summary>
        public event Action AbortAction;

        public FormProgressDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 中止按钮点击事件
        /// </summary>
        private void btn_Abort_Click(object sender, EventArgs e)
        {
            AbortAction?.Invoke();
            DialogResult = DialogResult.Abort;
            //Close();
        }

        /// <summary>
        /// 窗体载入事件
        /// </summary>
        private void FormProgressDialog_Load(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                OperateAction?.Invoke();
                DialogResult = DialogResult.OK;
            });
        }

        /// <summary>
        /// 设置显示信息(值为null时保持不变)
        /// </summary>
        /// <param name="rtfTitleContent">富文本格式的标题内容</param>
        /// <param name="totalMessage">总体消息</param>
        /// <param name="currentMessage">当前消息</param>
        public void SetInfo(string rtfTitleContent = null, string totalMessage = null, string currentMessage = null)
        {
            if (rtfTitleContent != null) rtb_Title.Rtf = rtfTitleContent;
            if (totalMessage != null) lbl_Total.Text = totalMessage;
            if (currentMessage != null) lbl_Current.Text = currentMessage;
        }

        /// <summary>
        /// 设置彩色标题内容
        /// </summary>
        /// <param name="text">文本</param>
        /// <param name="color">颜色</param>
        /// <param name="isNew">是否先清空</param>
        public void SetColorfulTitle(string text, Color color, bool isNew = false)
        {
            if (isNew) rtb_Title.Clear();
            rtb_Title.AppendTextColorful(text, color, false);
            rtb_Title.SelectionAlignment = HorizontalAlignment.Center;
        }

        /// <summary>
        /// 设置进度
        /// </summary>
        /// <param name="currentValue">当前数值</param>
        /// <param name="totalValue">总数值</param>
        public void SetProsess(double currentValue, double totalValue)
        {
            try
            {
                progressBar.Value = (int)(currentValue / totalValue * 100);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        #region 隐藏 RichTextBox 光标

        /// <summary>
        /// 隐藏光标
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("user32", EntryPoint = "HideCaret")]
        private static extern bool HideCaret(IntPtr hWnd);

        private void rtb_Title_MouseDown(object sender, MouseEventArgs e)
        {
            HideCaret(((RichTextBox)sender).Handle);
        }

        private void rtb_Title_MouseMove(object sender, MouseEventArgs e)
        {
            HideCaret(((RichTextBox)sender).Handle);
        }

        private void rtb_Title_KeyUp(object sender, KeyEventArgs e)
        {
            HideCaret(((RichTextBox)sender).Handle);
        }

        #endregion
    }
}
