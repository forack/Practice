﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;
using WPFPractice.Windows;

namespace WPFPractice
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Hyperlink_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Hyperlink link = sender as Hyperlink;
                string url = link.NavigateUri.AbsoluteUri;

                #region .NET Framework 方式

                var processStartInfo = new ProcessStartInfo(url);
                Process.Start(processStartInfo);

                #endregion

                #region //.NET Core 方式

                //Process process = new Process
                //{
                //    StartInfo =
                //    {
                //        FileName = "cmd.exe",
                //        UseShellExecute = false,            //不使用shell启动
                //        RedirectStandardInput = true,       //让cmd接受标准输入
                //        RedirectStandardOutput = false,     //不想听cmd讲话所以不要他输出
                //        RedirectStandardError = true,       //重定向标准错误输出
                //        CreateNoWindow = true,              //不显示窗口
                //    }
                //};

                //process.Start();

                ////向cmd窗口发送输入信息 后面的&exit告诉cmd运行好之后就退出
                //process.StandardInput.WriteLine($"start {url}&exit");
                //process.StandardInput.AutoFlush = true;
                //process.WaitForExit();//等待程序执行完退出进程
                //process.Close();

                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void BtnTextBox_OnClick(object sender, RoutedEventArgs e)
        {
            new WinTextBox().Show();
        }

        private void BtnSpirePdf_Click(object sender, RoutedEventArgs e)
        {
            new WinSpirePdf().Show();
        }

        private void BtnDingTalk_OnClick(object sender, RoutedEventArgs e)
        {
            new WinDingTalk().Show();
        }

        private void BtnMoveResize_OnClick(object sender, RoutedEventArgs e)
        {
            new WinButtonMoveResize().Show();
        }

        private void BtnReoGrid_OnClick(object sender, RoutedEventArgs e)
        {
            new WinReoGrid().Show();
        }

        private void BtnPathGeometry_OnClick(object sender, RoutedEventArgs e)
        {
            new WinPathGeometry().Show();
        }

        private void BtnGetLastInputInfo_OnClick(object sender, RoutedEventArgs e)
        {
            new WinGetLastInputInfo().Show();
        }

        private void BtnComboBox_OnClick(object sender, RoutedEventArgs e)
        {
            new WinComboBox().Show();
        }
    }
}
