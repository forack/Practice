﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practice
{
    public partial class AspNetPager_Test : System.Web.UI.Page
    {
        protected DataTable dt;
        protected const int NUM = 100;

        protected void Page_Load(object sender, EventArgs e)
        {
            dt = DataMaker.GetDataTable(new List<string>(){ "orderid", "companyname", "customerid", "employeename" }, NUM);
            AspNetPager1.RecordCount = NUM;
        }

        protected void AspNetPager1_OnPageChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void BindData()
        {
            DataTable dtResult = dt.Clone();
            DataRow[] rows = dt.Select("id >= " + AspNetPager1.StartRecordIndex + " and id <= " + AspNetPager1.EndRecordIndex);
            foreach (DataRow dr in rows)
            {
                dtResult.ImportRow(dr);
            }

            rep.DataSource = dtResult;
            rep.DataBind();
        }
    }
}