﻿namespace WinFormPractice
{
    partial class FormCkl
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ckl1 = new System.Windows.Forms.CheckedListBox();
            this.btnCheck = new System.Windows.Forms.Button();
            this.btnCheck2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ckl1
            // 
            this.ckl1.CheckOnClick = true;
            this.ckl1.FormattingEnabled = true;
            this.ckl1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.ckl1.Location = new System.Drawing.Point(53, 48);
            this.ckl1.Name = "ckl1";
            this.ckl1.Size = new System.Drawing.Size(125, 144);
            this.ckl1.TabIndex = 0;
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(220, 58);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(168, 49);
            this.btnCheck.TabIndex = 1;
            this.btnCheck.Text = "检查(有1和3)";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btnCheck2
            // 
            this.btnCheck2.Location = new System.Drawing.Point(220, 131);
            this.btnCheck2.Name = "btnCheck2";
            this.btnCheck2.Size = new System.Drawing.Size(168, 49);
            this.btnCheck2.TabIndex = 2;
            this.btnCheck2.Text = "检查(仅1和3)";
            this.btnCheck2.UseVisualStyleBackColor = true;
            this.btnCheck2.Click += new System.EventHandler(this.btnCheck2_Click);
            // 
            // FormCkl
            // 
            this.AcceptButton = this.btnCheck;
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 235);
            this.Controls.Add(this.btnCheck2);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.ckl1);
            this.Name = "FormCkl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormCheckListBox";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox ckl1;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Button btnCheck2;
    }
}

