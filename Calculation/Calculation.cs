﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic.CompilerServices;

namespace Calculation
{
    /// <summary>
    /// 计算
    /// 1.输入参数1：int flag，输入参数2：int、long、float、decimal
    /// 2.输出参数：类型和输入参数2相同，值根据flag的不同进行不同的操作
    /// flag=0的时候，返回参数2+10
    /// flag=1的时候，返回参数2-10
    /// flag=2的时候，返回参数2*10
    /// flag=3的时候，返回参数2/10
    /// flag等于其他值异常
    /// </summary>
    public class Calculation
    {
        public static int GetResult(int flag, int value)
        {
            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }

        public static long GetResult(int flag, long value)
        {
            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }

        public static float GetResult(int flag, float value)
        {
            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10.0F;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }

        public static decimal GetResult(int flag, decimal value)
        {
            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10.0M;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }
    }

    public class Calculation2
    {
        enum FlagEnum
        {
            加,减,乘,除
        }

        public static int GetResult(int flag, int value)
        {
            if (!Enum.TryParse(flag + "", out FlagEnum flagEnum))
            {
                throw new Exception("未定义的计算标识");
            }
            else
            {
                Console.Write($"{value} {flagEnum} 10 = ");
            }

            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }

        public static long GetResult(int flag, long value)
        {
            if (!Enum.TryParse(flag + "", out FlagEnum flagEnum))
            {
                throw new Exception("未定义的计算标识");
            }
            else
            {
                Console.Write($"{value} {flagEnum} 10 = ");
            }

            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }

        public static float GetResult(int flag, float value)
        {
            if (!Enum.TryParse(flag + "", out FlagEnum flagEnum))
            {
                throw new Exception("未定义的计算标识");
            }
            else
            {
                Console.Write($"{value} {flagEnum} 10 = ");
            }

            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10.0F;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }

        public static decimal GetResult(int flag, decimal value)
        {
            if (!Enum.TryParse(flag + "", out FlagEnum flagEnum))
            {
                throw new Exception("未定义的计算标识");
            }
            else
            {
                Console.Write($"{value} {flagEnum} 10 = ");
            }

            switch (flag)
            {
                case 0:
                    return value + 10;
                case 1:
                    return value - 10;
                case 2:
                    return value * 10;
                case 3:
                    return value / 10.0M;
                default:
                    throw new Exception("未定义的计算标识");
            }
        }
    }

    public class Calculation3
    {
        enum FlagEnum
        {
            加,减,乘,除
        }

        public static T GetResult<T>(int flag, T value)
            where T : struct
        {
            if (!Enum.TryParse(flag + "", out FlagEnum flagEnum))
            {
                throw new Exception("未定义的计算标识");
            }
            else
            {
                Console.Write($"{typeof(T)}：{value} {flagEnum} 10 = ");
            }

            dynamic v = value;

            switch (flag)
            {
                case 0:
                    return (T)(v + 10);
                case 1:
                    return (T)(v - 10);
                case 2:
                    return (T)(v * 10);
                case 3:
                    return (T)(v / 10);
                default:
                    throw new Exception("未定义的计算标识");
            }
        }
    }
}
