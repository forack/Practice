﻿namespace WinFormPractice
{
    partial class FormBrowserJs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.BtnSend = new System.Windows.Forms.Button();
            this.TBRecv = new System.Windows.Forms.TextBox();
            this.TBSend = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(12, 7);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(393, 435);
            this.webBrowser.TabIndex = 0;
            this.webBrowser.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // BtnSend
            // 
            this.BtnSend.Location = new System.Drawing.Point(417, 350);
            this.BtnSend.Name = "BtnSend";
            this.BtnSend.Size = new System.Drawing.Size(202, 23);
            this.BtnSend.TabIndex = 1;
            this.BtnSend.Text = "向网页发送消息";
            this.BtnSend.UseVisualStyleBackColor = true;
            this.BtnSend.Click += new System.EventHandler(this.BtnSend_Click);
            // 
            // TBRecv
            // 
            this.TBRecv.Location = new System.Drawing.Point(417, 50);
            this.TBRecv.Multiline = true;
            this.TBRecv.Name = "TBRecv";
            this.TBRecv.ReadOnly = true;
            this.TBRecv.Size = new System.Drawing.Size(201, 142);
            this.TBRecv.TabIndex = 2;
            // 
            // TBSend
            // 
            this.TBSend.Location = new System.Drawing.Point(417, 255);
            this.TBSend.Multiline = true;
            this.TBSend.Name = "TBSend";
            this.TBSend.Size = new System.Drawing.Size(201, 77);
            this.TBSend.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(415, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "网页传来的消息：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(415, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "向网页发送消息：";
            // 
            // FormBrowserJs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 454);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TBSend);
            this.Controls.Add(this.TBRecv);
            this.Controls.Add(this.BtnSend);
            this.Controls.Add(this.webBrowser);
            this.Name = "FormBrowserJs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormBrowserJs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Button BtnSend;
        private System.Windows.Forms.TextBox TBRecv;
        private System.Windows.Forms.TextBox TBSend;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}