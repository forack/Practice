﻿namespace WinFormPractice
{
    partial class Forms
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnSpirePdf = new System.Windows.Forms.Button();
            this.BtnBrowserJs = new System.Windows.Forms.Button();
            this.BtnCkl = new System.Windows.Forms.Button();
            this.BtnTbAndLbl = new System.Windows.Forms.Button();
            this.BtnProgressDialog = new System.Windows.Forms.Button();
            this.TBMessage = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // BtnSpirePdf
            // 
            this.BtnSpirePdf.Location = new System.Drawing.Point(12, 12);
            this.BtnSpirePdf.Name = "BtnSpirePdf";
            this.BtnSpirePdf.Size = new System.Drawing.Size(219, 23);
            this.BtnSpirePdf.TabIndex = 0;
            this.BtnSpirePdf.Text = "测试 Spire.Pdf";
            this.BtnSpirePdf.UseVisualStyleBackColor = true;
            this.BtnSpirePdf.Click += new System.EventHandler(this.BtnSpirePdf_Click);
            // 
            // BtnBrowserJs
            // 
            this.BtnBrowserJs.Location = new System.Drawing.Point(12, 41);
            this.BtnBrowserJs.Name = "BtnBrowserJs";
            this.BtnBrowserJs.Size = new System.Drawing.Size(219, 23);
            this.BtnBrowserJs.TabIndex = 1;
            this.BtnBrowserJs.Text = "测试 C# 通过 Browser 与 JS 交互";
            this.BtnBrowserJs.UseVisualStyleBackColor = true;
            this.BtnBrowserJs.Click += new System.EventHandler(this.BtnBrowserJs_Click);
            // 
            // BtnCkl
            // 
            this.BtnCkl.Location = new System.Drawing.Point(12, 70);
            this.BtnCkl.Name = "BtnCkl";
            this.BtnCkl.Size = new System.Drawing.Size(219, 23);
            this.BtnCkl.TabIndex = 2;
            this.BtnCkl.Text = "测试 CheckListBox 控件";
            this.BtnCkl.UseVisualStyleBackColor = true;
            this.BtnCkl.Click += new System.EventHandler(this.BtnCkl_Click);
            // 
            // BtnTbAndLbl
            // 
            this.BtnTbAndLbl.Location = new System.Drawing.Point(12, 98);
            this.BtnTbAndLbl.Name = "BtnTbAndLbl";
            this.BtnTbAndLbl.Size = new System.Drawing.Size(219, 23);
            this.BtnTbAndLbl.TabIndex = 3;
            this.BtnTbAndLbl.Text = "测试 TextBox 输入后在 Label 显示";
            this.BtnTbAndLbl.UseVisualStyleBackColor = true;
            this.BtnTbAndLbl.Click += new System.EventHandler(this.BtnTbAndLbl_Click);
            // 
            // BtnProgressDialog
            // 
            this.BtnProgressDialog.Location = new System.Drawing.Point(12, 127);
            this.BtnProgressDialog.Name = "BtnProgressDialog";
            this.BtnProgressDialog.Size = new System.Drawing.Size(219, 23);
            this.BtnProgressDialog.TabIndex = 4;
            this.BtnProgressDialog.Text = "测试 进度条任务弹窗";
            this.BtnProgressDialog.UseVisualStyleBackColor = true;
            this.BtnProgressDialog.Click += new System.EventHandler(this.BtnProgressDialog_Click);
            // 
            // TBMessage
            // 
            this.TBMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBMessage.Location = new System.Drawing.Point(252, 13);
            this.TBMessage.Multiline = true;
            this.TBMessage.Name = "TBMessage";
            this.TBMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TBMessage.Size = new System.Drawing.Size(321, 216);
            this.TBMessage.TabIndex = 5;
            // 
            // Forms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 241);
            this.Controls.Add(this.TBMessage);
            this.Controls.Add(this.BtnProgressDialog);
            this.Controls.Add(this.BtnTbAndLbl);
            this.Controls.Add(this.BtnCkl);
            this.Controls.Add(this.BtnBrowserJs);
            this.Controls.Add(this.BtnSpirePdf);
            this.Name = "Forms";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Forms";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnSpirePdf;
        private System.Windows.Forms.Button BtnBrowserJs;
        private System.Windows.Forms.Button BtnCkl;
        private System.Windows.Forms.Button BtnTbAndLbl;
        private System.Windows.Forms.Button BtnProgressDialog;
        private System.Windows.Forms.TextBox TBMessage;
    }
}

