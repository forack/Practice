﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
/*
 * https://gitee.com/dlgcy/dotnetcodes
 */
namespace DotNet.Utilities.WpfHelpers
{
    /// <summary>
    /// WPF绑定属性基类;
    /// </summary>
    public abstract class BindableBase : INotifyPropertyChanged
    {
        #region BindableBase

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] String propertyName = null)
        {
            if (Equals(storage, value)) return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        #endregion

        /// <summary>
        /// 示例
        /// </summary>
        class Sample : BindableBase
        {
            //private List<string> _stuList;
            //public List<string> StuList
            //{
            //    get => _stuList;
            //    set => SetProperty(ref _stuList, value);
            //}
        }
    }
}
