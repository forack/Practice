﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DAL;
using DLGCY.Utilities;
using Newtonsoft.Json;

namespace Practice.ashx
{
    /// <summary>
    /// GetData 的摘要说明
    /// </summary>
    public class GetData : IHttpHandler
    {
        private int total = 0; // 总数据条数；
        private string data = ""; // 返回的数据；
        private string msg = "未能获取到数据"; // 返回的消息；

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "application/json";
                DataTable table = GetDatas(context);
                
                data = JsonConvert.SerializeObject(table);
                msg = "1";
            }
            catch (Exception ex)
            {
                msg = "获取信息失败！" + ex;
            }
            context.Response.Write("{\"data\":" + data + ",\"total\":" + total + ",\"msg\":\"" + msg + "\"}");
        }

        /// <summary>
        /// 获取数据
        /// </summary>      
        public DataTable GetDatas(HttpContext context)
        {
            int page = Convert.ToInt32(context.Request["curPage"]);
            int pageCount = Convert.ToInt32(context.Request["perPageCount"]);
            
            DataTable dtOrigion = DataMaker.GetDataTable(
                new List<string>() { "orderid", "companyname", "customerid", "employeename" }, 98);
            total = dtOrigion.Rows.Count;

            return DataTableHelper.GetPagedDataTable(page, pageCount, dtOrigion);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}