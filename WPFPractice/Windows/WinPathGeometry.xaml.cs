﻿using DotNet.Utilities.WpfHelpers;
using PropertyChanged;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace WPFPractice.Windows
{
    /// <summary>
    /// Path 几何图形
    /// </summary>
    public partial class WinPathGeometry : Window
    {
        private PathGeometryViewModel _VM;

        public WinPathGeometry()
        {
            InitializeComponent();
            _VM = new PathGeometryViewModel();
            DataContext = _VM;
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class PathGeometryViewModel : BindableBase
    {
        #region 成员

        /// <summary>
        /// 状态信息
        /// </summary>
        public string StatusInfo { get; set; }

        #endregion

        public PathGeometryViewModel()
        {
            SetCommandMethod();
        }

        #region 辅助方法

        public void ShowInfo(string info)
        {
            StatusInfo = $"{info}";
        }

        #endregion

        #region 命令

        public ICommand AddBtnCommand { get; set; }

        public ICommand DelBtnCommand { get; set; }

        #endregion

        /// <summary>
        /// 命令方法赋值（在构造方法中调用）
        /// </summary>
        private void SetCommandMethod()
        {
            AddBtnCommand ??= new RelayCommand(o => true, async o =>
            {

            });

            DelBtnCommand ??= new RelayCommand(o => true, async o =>
            {

            });
        }
    }
}
