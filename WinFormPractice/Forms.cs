﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DotNet.Utilities.WinFormHelper;
using Wlh.TextBoxLabel;

namespace WinFormPractice
{
    public partial class Forms : Form
    {
        public Forms()
        {
            InitializeComponent();
        }

        private void ShowInfo(string messgae)
        {
            TBMessage.TryBeginInvoke(new Action(() =>
            {
                TBMessage.AppendText($"{messgae}\r\n\r\n");
            }));
        }

        private void BtnSpirePdf_Click(object sender, EventArgs e)
        {
            new FormSpirePdf().Show();
        }

        private void BtnBrowserJs_Click(object sender, EventArgs e)
        {
            new FormBrowserJs().Show();
        }

        private void BtnCkl_Click(object sender, EventArgs e)
        {
            new FormCkl().Show();
        }

        private void BtnTbAndLbl_Click(object sender, EventArgs e)
        {
            new FormTbAndLbl().Show();
        }

        #region 测试任务进度条弹窗

        private CancellationTokenSource _Cts; //任务取消令牌;
        private AutoResetEvent _AutoResetEvent = new AutoResetEvent(false);//参数传 false，则 WaitOne 时阻塞等待;

        /// <summary>
        /// 测试任务进度弹窗
        /// </summary>
        private void BtnProgressDialog_Click(object sender, EventArgs e)
        {
            _AutoResetEvent.Reset();
            string businessName = "业务1";

            FormProgressDialog progressWindow = new FormProgressDialog()
            {
                Text = "任务处理窗口",
            };

            progressWindow.SetColorfulTitle("业务1 ", Color.DarkOrange, true);
            progressWindow.SetColorfulTitle("正在执行中......", Color.Black);
            progressWindow.SetInfo(null, "", "");

            List<string> orders = new List<string>(){"订单1", "订单2", "订单3", "订单4", "订单5" }; //业务数据;
            List<string> leftList = orders.Select(x => x).ToList(); //剩余（未处理）数据;
            int successCount = 0; //成功数量;

            _Cts = new CancellationTokenSource();

            //注册一个将在取消此 CancellationToken 时调用的委托;
            _Cts.Token.Register(async () =>
            {
                ShowInfo("操作终止");
                
                await Task.Run(() =>
                {
                    _AutoResetEvent.WaitOne(1000 * 5); //等待有可能还在执行的业务方法;

                    if (successCount < orders.Count)
                    {
                        ShowInfo($"{businessName} 有 {orders.Count - successCount} 项任务被终止，可在消息框中查看具体项。");

                        foreach (var leftName in leftList)
                        {
                            ShowInfo($"【{businessName}】的【{leftName}】执行失败，失败原因：【手动终止】。");
                        }
                    }
                });
                
            });

            progressWindow.OperateAction += () =>
            {
                Task task = new Task(() =>
                {
                    foreach (var order in orders)
                    {
                        //判断是否被取消;
                        if (_Cts.Token.IsCancellationRequested)
                        {
                            break;
                        }

                        progressWindow.TryBeginInvoke(new Action(() =>
                        {
                            progressWindow.SetInfo(null, $"共{orders.Count}项，已执行{successCount}项", $"当前正在执行：{order}");
                        }));

                        if (BusinessMethod(order, businessName))
                        {
                            successCount++;
                            leftList.RemoveAll(x => x == order);

                            if (_Cts.Token.IsCancellationRequested)
                            {
                                _AutoResetEvent.Set(); //放行 Register 委托处的等待;
                            }
                        }

                        progressWindow.TryBeginInvoke(new Action(() =>
                        {
                            progressWindow.SetProsess(orders.IndexOf(order) + 1, orders.Count);
                        }));
                    }

                    //正常结束则延迟一段时间来让进度条走完;
                    if (!_Cts.Token.IsCancellationRequested)
                    {
                        Thread.Sleep(200);
                    }
                }, _Cts.Token);

                task.Start();
                task.Wait();
            };

            progressWindow.AbortAction += () =>
            {
                _Cts.Cancel();
            };

            var result = progressWindow.ShowDialog();
            int leftCount = orders.Count - successCount;
            if (result == DialogResult.OK || leftCount <= 0)
            {
                ShowInfo($"{businessName} 整体完成。");
            }
            else if (result == DialogResult.Abort)
            {
                //移到 _Cts.Token.Register 处一起判断，不然数目可能不准;
                //ShowInfo($"{businessName} 有 {leftCount} 项任务被终止，可在消息框中查看具体项。");
            }
        }

        /// <summary>
        /// 业务处理方法
        /// </summary>
        private bool BusinessMethod(string order, string businessName)
        {
            string errStr = $"【{businessName}】的 {order} 任务失败，失败原因：";

            //测试
            Thread.Sleep(1000 * 2);

            try
            {
                //业务方法;

                ShowInfo($"【{businessName}】的 {order} 任务执行成功。");
                return true;
            }
            catch (Exception ex)
            {
                ShowInfo($"{errStr}{ex.Message}");
            }

            return false;
        }

        #endregion
    }
}
