﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practice
{
    public partial class GradientChange : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Data_Binding();
        }

        Dictionary<int, int> Datas
        {
            get
            {
                Dictionary<int, int> d = new Dictionary<int, int>();
                d.Add(1, 35);
                d.Add(2, 45);
                d.Add(3, 20);
                return d;
            }
        }

        private void Data_Binding()
        {
            int totals = 100;
            foreach (KeyValuePair<int, int> kvp in Datas)
            {
                double rate = kvp.Value / (double)totals;

                double width = rate * 300;
                switch (kvp.Key)
                {
                    case 1:
                        this.Label1.Text = GradientImage(width, rate);
                        break;
                    case 2:
                        this.Label2.Text = GradientImage(width, rate);
                        break;
                    case 3:
                        this.Label3.Text = GradientImage(width, rate);
                        break;
                }
            }
        }

        private string GradientImage(double width, double rate)
        {
            return "<IMG height='21' src='images/bar_green.png' width='" + width + "' align='absMiddle'> " + rate.ToString("p");
        }
    }
}