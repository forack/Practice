﻿namespace WinFormPractice
{
    partial class FormSpirePdf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnAddBookmarks = new System.Windows.Forms.Button();
            this.BtnAddSubBookmarks = new System.Windows.Forms.Button();
            this.BtnAddToFile = new System.Windows.Forms.Button();
            this.BtnModify = new System.Windows.Forms.Button();
            this.BtnInsert = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnCrack = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.BtnChooseFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TBPath = new System.Windows.Forms.TextBox();
            this.TVBookmarks = new System.Windows.Forms.TreeView();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnAddBookmarks
            // 
            this.BtnAddBookmarks.Location = new System.Drawing.Point(13, 26);
            this.BtnAddBookmarks.Name = "BtnAddBookmarks";
            this.BtnAddBookmarks.Size = new System.Drawing.Size(75, 23);
            this.BtnAddBookmarks.TabIndex = 0;
            this.BtnAddBookmarks.Text = "添加书签";
            this.BtnAddBookmarks.UseVisualStyleBackColor = true;
            this.BtnAddBookmarks.Click += new System.EventHandler(this.BtnAddBookmarks_Click);
            // 
            // BtnAddSubBookmarks
            // 
            this.BtnAddSubBookmarks.Location = new System.Drawing.Point(113, 26);
            this.BtnAddSubBookmarks.Name = "BtnAddSubBookmarks";
            this.BtnAddSubBookmarks.Size = new System.Drawing.Size(75, 23);
            this.BtnAddSubBookmarks.TabIndex = 1;
            this.BtnAddSubBookmarks.Text = "添加子书签";
            this.BtnAddSubBookmarks.UseVisualStyleBackColor = true;
            this.BtnAddSubBookmarks.Click += new System.EventHandler(this.BtnAddSubBookmarks_Click);
            // 
            // BtnAddToFile
            // 
            this.BtnAddToFile.Location = new System.Drawing.Point(213, 26);
            this.BtnAddToFile.Name = "BtnAddToFile";
            this.BtnAddToFile.Size = new System.Drawing.Size(133, 23);
            this.BtnAddToFile.TabIndex = 2;
            this.BtnAddToFile.Text = "添加书签到现有文档";
            this.BtnAddToFile.UseVisualStyleBackColor = true;
            this.BtnAddToFile.Click += new System.EventHandler(this.BtnAddToFile_Click);
            // 
            // BtnModify
            // 
            this.BtnModify.Location = new System.Drawing.Point(371, 26);
            this.BtnModify.Name = "BtnModify";
            this.BtnModify.Size = new System.Drawing.Size(75, 23);
            this.BtnModify.TabIndex = 3;
            this.BtnModify.Text = "修改书签";
            this.BtnModify.UseVisualStyleBackColor = true;
            this.BtnModify.Click += new System.EventHandler(this.BtnModify_Click);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Location = new System.Drawing.Point(471, 26);
            this.BtnInsert.Name = "BtnInsert";
            this.BtnInsert.Size = new System.Drawing.Size(75, 23);
            this.BtnInsert.TabIndex = 4;
            this.BtnInsert.Text = "插入书签";
            this.BtnInsert.UseVisualStyleBackColor = true;
            this.BtnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(571, 26);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(75, 23);
            this.BtnDelete.TabIndex = 5;
            this.BtnDelete.Text = "删除书签";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.BtnCrack);
            this.groupBox1.Controls.Add(this.BtnAddSubBookmarks);
            this.groupBox1.Controls.Add(this.BtnDelete);
            this.groupBox1.Controls.Add(this.BtnAddBookmarks);
            this.groupBox1.Controls.Add(this.BtnInsert);
            this.groupBox1.Controls.Add(this.BtnAddToFile);
            this.groupBox1.Controls.Add(this.BtnModify);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 67);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "测试区";
            // 
            // BtnCrack
            // 
            this.BtnCrack.Location = new System.Drawing.Point(665, 26);
            this.BtnCrack.Name = "BtnCrack";
            this.BtnCrack.Size = new System.Drawing.Size(75, 23);
            this.BtnCrack.TabIndex = 6;
            this.BtnCrack.Text = "破解";
            this.BtnCrack.UseVisualStyleBackColor = true;
            this.BtnCrack.Click += new System.EventHandler(this.BtnCrack_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "*.pdf";
            // 
            // BtnChooseFile
            // 
            this.BtnChooseFile.Location = new System.Drawing.Point(677, 93);
            this.BtnChooseFile.Name = "BtnChooseFile";
            this.BtnChooseFile.Size = new System.Drawing.Size(111, 23);
            this.BtnChooseFile.TabIndex = 9;
            this.BtnChooseFile.Text = "选择文件";
            this.BtnChooseFile.UseVisualStyleBackColor = true;
            this.BtnChooseFile.Click += new System.EventHandler(this.BtnChooseFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "文件路径：";
            // 
            // TBPath
            // 
            this.TBPath.Location = new System.Drawing.Point(83, 95);
            this.TBPath.Name = "TBPath";
            this.TBPath.ReadOnly = true;
            this.TBPath.Size = new System.Drawing.Size(575, 21);
            this.TBPath.TabIndex = 7;
            // 
            // TVBookmarks
            // 
            this.TVBookmarks.Location = new System.Drawing.Point(14, 139);
            this.TVBookmarks.Name = "TVBookmarks";
            this.TVBookmarks.Size = new System.Drawing.Size(247, 299);
            this.TVBookmarks.TabIndex = 10;
            // 
            // FormSpirePdf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TVBookmarks);
            this.Controls.Add(this.BtnChooseFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TBPath);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormSpirePdf";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSpirePdf（见 WPFPractice 项目）";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnAddBookmarks;
        private System.Windows.Forms.Button BtnAddSubBookmarks;
        private System.Windows.Forms.Button BtnAddToFile;
        private System.Windows.Forms.Button BtnModify;
        private System.Windows.Forms.Button BtnInsert;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button BtnChooseFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TBPath;
        private System.Windows.Forms.TreeView TVBookmarks;
        private System.Windows.Forms.Button BtnCrack;
    }
}