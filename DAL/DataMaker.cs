﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DataMaker
    {
        public static DataTable GetDataTable(List<string> itemList, int num)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(int)));

            foreach (string item in itemList)
            {
                dt.Columns.Add(new DataColumn(item, typeof(string)));
            }

            DataRow dr;
            for (int i = 1; i <= num; i++)
            {
                dr = dt.NewRow();
                dr[0] = i;
                foreach (string str in itemList)
                {
                    dr[str] = str + i.ToString();
                }
                dt.Rows.Add(dr);
            }

            return dt;
        }
    }
}
